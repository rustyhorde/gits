extern crate git2;

use git2::Repository;

fn main() {
    if let Ok(repo) = Repository::open("/home/projects/gits") {
        if let Ok(remotes) = repo.remotes() {
            for rem_opt in remotes.iter() {
                if let Some(remote) = rem_opt {
                    println!("Remote: {}", remote);
                }
            }
        }
    }
    println!("Hello, world!");
}
